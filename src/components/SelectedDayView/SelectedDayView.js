import { observer } from 'mobx-react-lite';
import React from 'react';
import { useStore } from '../../hooks/useStore';
import { useForecast } from '../../hooks/useForecast';

export const SelectedDayView = observer(() => {
    // eslint-disable-next-line no-undef
    const { data: days } = useForecast();
    const store = useStore();
    const { isFiltered, selectedDayId } = store;
    if (typeof days === 'undefined') {
        return null;
    }
    const data = Array.isArray(days) && isFiltered
        ? store.filteredDays(days)
        : days;
    let today = data[ 0 ];
    const weekday = (num) => {
        return ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'][ new Date(num).getDay() ];
    };
    // eslint-disable-next-line array-callback-return
    data.map((value, index) => {
        if (value.id === selectedDayId) {
            return (
                today = data[ index ]
            );
        }
    });

    return (
        <>
            <div className = 'head'>
                <div className = { `icon ${today?.type}` } />
                <div className = 'current-date'>
                    <p>{ weekday(today?.day) }</p>
                    <span>
                        27
                        { /* { format(new Date(Number(today?.day)), 'dd', { locale: ru }) } { format(new Date(Number(today?.day)), 'LLLL', { locale: ru }) } */ }
                    </span>
                </div>
            </div>
            <div className = 'current-weather'>
                <p className = 'temperature'>{ today?.temperature }</p>
                <p className = 'meta'>
                    <span className = 'rainy'>%{ today?.rain_probability }</span>
                    <span className = 'humidity'>%{ today?.humidity }</span>
                </p>
            </div>
        </>
    );
});
