// Core
import * as yup from 'yup';

export const schema = yup.object.shape({
    age: yup.number().required().positive().integer(),
});
