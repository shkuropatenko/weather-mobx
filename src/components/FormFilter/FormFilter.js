// Core
import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies,node/no-unpublished-import
import { useForm } from 'react-hook-form';
import { observer } from 'mobx-react-lite';
import cx from 'classnames';

// Hooks
import { useStore } from '../../hooks/useStore';

const BtnText = {
    submit: 'Отфильтровать',
    reset:  'Сбросить',
};

export const FormFilter = observer(() => {
    const store = useStore();
    const filterButtonJSX = store.isFiltered ? BtnText.reset : BtnText.submit;
    const form = useForm({
        mode: 'onSubmit',
    });

    const cloudyWeatherCX = cx('checkbox', {
        selected: store.type === 'cloudy',
        blocked:  store.isFiltered,
    });

    const sunnyWeatherCX = cx('checkbox', {
        selected: store.type === 'sunny',
        blocked:  store.isFiltered,
    });

    const handleMinTemperatureChange = (event) => {
        const { value } = event.target;
        form.setValue('minTemperature', value);
        store.setMinTemperature(value);
    };

    const handleMaxTemperatureChange = (event) => {
        const { value } = event.target;
        form.setValue('maxTemperature', value);
        store.setMaxTemperature(value);
    };

    // eslint-disable-next-line require-await
    const onSubmit = form.handleSubmit(async (filterData) => {
        if (filterButtonJSX === BtnText.submit) {
            store.applyFilter(filterData);
        } else if (filterButtonJSX === BtnText.reset) {
            form.reset();
            store.resetFilter();
        }
    });

    return (
        <form className = 'filter' onSubmit = { onSubmit } >
            <span
                className = { cloudyWeatherCX } onClick = { () => {
                    form.setValue('type', 'cloudy');
                    store.setType('cloudy');
                } }>
               Облачно
            </span>
            <span
                className = { sunnyWeatherCX } onClick = { () => {
                    form.setValue('type', 'sunny');
                    store.setType('sunny');
                } }>
               Солнечно
            </span>

            <p className = 'custom-input'>
                <label htmlFor = 'min-temperature'>Минимальная температура</label>
                <input
                    id = 'min-temperature'
                    type = 'text'
                    onBlur = { handleMinTemperatureChange }
                    value = { store.inputValue } />
            </p>
            <p className = 'custom-input'>
                <label htmlFor = 'min-temperature'>Максимальная температура</label>
                <input
                    id = 'max-temperature'
                    type = 'text'
                    onBlur = { handleMaxTemperatureChange }
                    value = { store.inputValue } />
            </p>
            <button type = 'submit' disabled = { store.isFormBlocked }>
                { filterButtonJSX }
            </button>
        </form>
    );
});

