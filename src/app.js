import React, { useState } from 'react';
import { observer } from 'mobx-react-lite';
import { useForecast } from './hooks/useForecast';
import { FormFilter } from './components/FormFilter/FormFilter';
import { useStore } from './hooks/useStore';
import { SelectedDayView } from './components/SelectedDayView/SelectedDayView';

// Other
export const App = observer(() => {
    const { data: days, rootStore } = useForecast();
    const store = useStore();
    const { isFiltered } = store;

    if (typeof days === 'undefined') {
        return null;
    }

    const data = Array.isArray(days) && isFiltered
        ? store.filteredDays(days)
        : days;
    const [currentDay, setCurrentDay] = useState(days[ 0 ]);
    const handleSettingsClick = (event) => {
        setCurrentDay(event.target.id);
        store.setSelectedDayId(event.target.id);
    };

    const weekday = (num) => {
        return ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'][ new Date(num).getDay() ];
    };

    const daysWeek = data.slice(0, 7).map(({
        day,
        id,
        temperature,
        type,
    }) => {
        return (
            <div
                className = { `day ${type}` }
                key = { id }
                id = { id }
                onClick = {  handleSettingsClick }>
                <p>{ weekday(day) }</p>
                <span>{ temperature }</span>
            </div>
        );
    });

    return (
        <main>
            <FormFilter />
            <SelectedDayView />
            <div className = 'forecast'>
                { daysWeek }
            </div>
        </main>
    );
});

