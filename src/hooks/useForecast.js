// Core
import { useQuery } from 'react-query';

// Other
import { api } from '../api';
import { useStore } from './useStore';

export const useForecast = () => {
    const rootStore = useStore();
    const query = useQuery('days', api.getWeather);
    const { data, isFetchedAfterMount  } = query;

    return {
        data: Array.isArray(data) ? data : [],
        isFetchedAfterMount,
        rootStore,
    };
};
