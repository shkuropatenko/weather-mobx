import { useEffect } from 'react';
import { useForecast } from './useForecast';
import { useStore } from './useStore';

export const useSelectDay = () => {
    const { data, isFetchedAfterMount } = useForecast();
    const store = useStore();

    useEffect(() => {
        if (typeof data !== 'undefined') {
            store.setSelectedDayId(data[ 0 ].id);
        }
    }, [isFetchedAfterMount]);

    if (!store.selectedDayId) {
        return null;
    }

    let selectedDay = data?.find(({ id }) => id === store.selectedDayId);

    if (!selectedDay) {
        return null;
    }

    if (store.isFiltered) {
        const filtered = store.filteredDays(data);

        selectedDay = filtered.find(({ id }) => id === store.selectedDayId) || filtered[ 0 ];
    }

    return selectedDay;
};
